﻿USE sample;

IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'i_works_on_enter_date')
BEGIN
    DROP INDEX [works_on].[i_works_on_enter_date];
END

IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'i_employee_lname')
BEGIN
    DROP INDEX [employee].[i_employee_lname];
END

IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'i_employee_lname_fname')
BEGIN
    DROP INDEX [employee].[i_employee_lname_fname];
END

IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'i_works_on_emp_no')
BEGIN
    DROP INDEX [employee].[i_works_on_emp_no];
END

IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'i_employee_emp_no')
BEGIN
    DROP INDEX [employee].[i_employee_emp_no];
END

IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'i_department_dept_no')
BEGIN
    DROP INDEX [employee].[i_department_dept_no];
END

IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'i_department_dept_name')
BEGIN
    DROP INDEX [employee].[i_department_dept_name];
END

IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'i_employee_dept_no')
BEGIN
    DROP INDEX [employee].[i_employee_dept_no];
END

CREATE NONCLUSTERED INDEX i_works_on_enter_date ON [works_on]([enter_date]) WITH (FILLFACTOR = 70);

-- SELECT emp_no, emp_fname, emp_lname FROM employee WHERE emp_lname = 'Smith' 
CREATE NONCLUSTERED INDEX i_employee_lname ON [employee]([emp_lname]);

-- SELECT emp_no, emp_fname, emp_lname FROM employee WHERE emp_lname = 'Hansel' AND emp_fname = 'Elke'
CREATE NONCLUSTERED INDEX i_employee_lname_fname ON [employee]([emp_lname],[emp_fname]);

-- SELECT job FROM works_on, employee WHERE employee.emp_no = works_on.emp_no
CREATE NONCLUSTERED INDEX i_works_on_emp_no ON [works_on]([emp_no]);
CREATE NONCLUSTERED INDEX i_employee_emp_no ON [employee]([emp_no]);

-- SELECT emp_lname, emp_fname FROM employee, department WHERE employee.dept_no = department.dept_no AND dept_name = 'Research'
CREATE NONCLUSTERED INDEX i_department_dept_no ON [department]([dept_no]);
CREATE NONCLUSTERED INDEX i_department_dept_name ON [department]([dept_name]);
CREATE NONCLUSTERED INDEX i_employee_dept_no ON [employee]([dept_no]);