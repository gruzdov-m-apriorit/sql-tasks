using System.Data.SqlClient;
using System.Transactions;
using Microsoft.SqlServer.Server;

public partial class Triggers
{
    // Enter existing table or view for the target and uncomment the attribute line
    [SqlTrigger(Name = "refint_workson2", Target = "employee", Event = "AFTER DELETE, UPDATE")]
    public static void refint_workson2()
    {
        if (!SqlContext.TriggerContext.IsUpdatedColumn(0))
        {
            return;
        }

        using (var connection = new SqlConnection(@"context connection=true"))
        {
            connection.Open();
            using (var countCommand = new SqlCommand("SELECT COUNT(*) FROM works_on, deleted WHERE works_on.emp_no = deleted.emp_no", connection))
            {
                int count = (int)countCommand.ExecuteScalar();

                if (count > 0)
                {
                    Transaction.Current.Rollback();
                    SqlContext.Pipe.Send("No modification/deletion of the row");
                }
                else
                {
                    SqlContext.Pipe.Send("The row is deleted/modified");
                }
            }
        }
    }
}

