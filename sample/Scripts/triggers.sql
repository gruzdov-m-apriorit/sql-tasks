﻿-- 14.1
GO
CREATE TRIGGER tr_employee_dept_no_insert
ON employee AFTER UPDATE, INSERT 
AS
IF UPDATE(dept_no)
BEGIN
    IF (SELECT department.dept_no FROM department, inserted WHERE department.dept_no = inserted.dept_no) IS NULL
    BEGIN
        ROLLBACK TRANSACTION
        PRINT 'Cannot insert/update employee for not-existing department'
    END
END

GO
CREATE TRIGGER tr_department_dept_no_update
ON department AFTER UPDATE, DELETE 
AS
IF UPDATE(dept_no)
BEGIN
    IF (SELECT COUNT(*) FROM employee, deleted WHERE employee.dept_no = deleted.dept_no) > 0
    BEGIN
        ROLLBACK TRANSACTION
        PRINT 'Cannot delete/update department when it has active employees'
    END
END

-- 14.2
GO
CREATE TRIGGER tr_works_on_project_no_insert
ON works_on AFTER UPDATE, INSERT 
AS
IF UPDATE(project_no)
BEGIN
    IF (SELECT project.project_no FROM project, inserted WHERE project.project_no = inserted.project_no) IS NULL
    BEGIN
        ROLLBACK TRANSACTION
        PRINT 'Cannot insert/update work for not-existing project'
    END
END

GO
CREATE TRIGGER tr_project_project_no_update
ON project AFTER UPDATE, DELETE 
AS
IF UPDATE(project_no)
BEGIN
    IF (SELECT COUNT(*) FROM works_on, deleted WHERE works_on.project_no = deleted.project_no) > 0
    BEGIN
        ROLLBACK TRANSACTION
        PRINT 'Cannot inser/update project when it has active works'
    END
END

-- 14.3
/*
See 14-3-trigger.cs
*/
GO
CREATE ASSEMBLY trigger_14_3 FROM 'sample\bin\debug\sample.dll' WITH PERMISSION_SET = EXTERNAL_ACCESS

GO
CREATE TRIGGER refint_workson2 ON employee AFTER DELETE, UPDATE
AS
EXTERNAL NAME trigger_14_3.Triggers.refint_workson2
