﻿USE sample;

GO
-- 11.1
CREATE VIEW v_employee_d1
AS SELECT * FROM [employee]
WHERE dept_no = 'd1';

GO

-- 11.2
CREATE VIEW v_project
AS SELECT project_no, project_name FROM [project];

GO

-- 11.3
CREATE VIEW v_works_on_2007
AS SELECT emp.[emp_fname], emp.[emp_lname]
FROM [works_on] wo
INNER JOIN [employee] emp ON wo.[emp_no] = emp.[emp_no]
WHERE wo.[enter_date] BETWEEN '2007-06-01' AND '2007-12-31';

GO

-- 11.4
CREATE VIEW v_works_on_2007_alt (first, last)
AS SELECT [emp_fname], [emp_lname]
FROM v_works_on_2007;

GO 

-- 11.5
CREATE VIEW v_emp_m
AS SELECT * FROM [employee]
WHERE emp_lname LIKE 'M%';

GO
-- 11.6
CREATE VIEW v_project_smith
AS SELECT pro.* FROM [works_on] wo
INNER JOIN [employee] emp ON wo.[emp_no] = emp.[emp_no]
INNER JOIN [project] pro ON pro.[project_no] = wo.[project_no]
WHERE emp.[emp_lname] = 'Smith';

GO

-- 11.7
ALTER VIEW v_employee_d1
AS SELECT * FROM [employee]
WHERE dept_no IN ('d1', 'd2');

GO

-- 11.8 (also removes v_works_on_2007_alt)
DROP VIEW v_works_on_2007;

GO

-- 11.9
INSERT INTO v_project (project_no, project_name)
VALUES ('p2', 'Moon');

GO

-- 11.10
CREATE VIEW v_emp_10000
AS SELECT * FROM [employee] WHERE [emp_no] < 10000
WITH CHECK OPTION;

GO

-- will fail (22123 > 10000)
-- INSERT INTO v_emp_10000 (emp_lname, emp_no, dept_no)
-- VALUES ('Kohn', 22123, 'd3');

GO

-- 11.11
CREATE VIEW v_emp_10000_nocheck
AS SELECT * FROM [employee] WHERE [emp_no] < 10000;

GO

-- will insert ok
INSERT INTO v_emp_10000_nocheck (emp_lname, emp_no, dept_no)
VALUES ('Kohn', 22123, 'd3');

GO

-- 11.12
CREATE VIEW v_works_on_2007_2008
AS SELECT * FROM [works_on]
WHERE [enter_date] BETWEEN '2007-01-01' AND '2008-12-31'
WITH CHECK OPTION;

GO

-- won't work (2006 < 2007)
-- UPDATE [v_works_on_2007_2008] SET [enter_date] = '2006-06-01' WHERE [emp_no] = '29346';

-- 11.13
CREATE VIEW v_works_on_2007_2008_nocheck
AS SELECT * FROM [works_on]
WHERE [enter_date] BETWEEN '2007-01-01' AND '2008-12-31';

GO

-- will update ok
UPDATE [v_works_on_2007_2008_nocheck] SET [enter_date] = '2006-06-01' WHERE [emp_no] = '29346';

GO;