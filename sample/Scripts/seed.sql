﻿IF NOT EXISTS (SELECT 1 FROM [department])
BEGIN
INSERT INTO [department] (dept_no, dept_name, location)
VALUES
('d1', 'Research', 'Dallas'),
('d2', 'Accounting', 'Seattle'),
('d3', 'Marketing', 'Dallas');
END;

IF NOT EXISTS (SELECT 1 FROM [employee])
BEGIN
INSERT INTO [employee] (emp_no, emp_fname, emp_lname, dept_no)
VALUES
(25348, 'Matthew', 'Smith', 'd3'),
(10102, 'Ann', 'Jones', 'd3'),
(18316, 'John', 'Barrimore', 'd1'),
(29346, 'James', 'James', 'd2'),
(9031, 'Elsa', 'Bertoni', 'd2'),
(2581, 'Elke', 'Hansel', 'd2'),
(28559, 'Sybill', 'Moser', 'd1');
END;

IF NOT EXISTS (SELECT 1 FROM [project])
BEGIN
INSERT INTO [project] (project_no, project_name, budget)
VALUES
('p1', 'Apollo', 120000),
('p2', 'Gemini', 95000),
('p3', 'Mercury', 185600);
END;

IF NOT EXISTS (SELECT 1 FROM [works_on])
BEGIN
INSERT INTO [works_on] (emp_no, project_no, job, enter_date)
VALUES
(10102, 'p1', 'Analyst', '2006.10.1'),
(10102, 'p3', 'Manager', '2008.1.1'),
(25348, 'p2', 'Cork','2007.2.15'),
(18316, 'p2', NULL, '2007.6.1'),
(29346, 'p2', NULL, '2006.12.15'),
(2581, 'p3', 'Analyst', '2007.10.15'),
(9031, 'p1', 'Manager', '2007.4.15'),
(28559, 'p1', NULL, '2007.8.1'),
(28559, 'p2', 'Clerk', '2008.2.1'),
(9031, 'p3', 'Clerk', '2006.11.15'),
(29346, 'p1', 'Clerk', '2007.1.4');
END;