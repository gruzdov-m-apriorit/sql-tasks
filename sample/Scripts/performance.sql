-- 20.1 Опишите различия между приложением SQL Server Profiler и помощником Database Engine Tuning Advisor
/*
SQL Server Profiler собирает и записыват статистические данные, которые затем 
можно обработать с помощью Database Engine Tuning Advisor.
*/

-- 20.2 Какие имеются различия между сборщиком данных Performance Data Collector и регулятором ресурсов Resource Governor? 
/*
Performance Data Collector хранит данные и строит отчеты по производительности,
а с помощью Resource Governor можно настроить распределение ресурсов по группам и пулам.
*/