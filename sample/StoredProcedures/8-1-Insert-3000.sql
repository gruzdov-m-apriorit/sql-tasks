﻿GO

CREATE PROCEDURE [insert_employees]
    @count INT = 3000
AS
BEGIN
    DECLARE @index INT = 0;
    WHILE @index < @count
    BEGIN
        INSERT [employee] (emp_no, emp_fname, emp_lname, dept_no)
        VALUES (@index, 'Jane', 'Smith', 'd1');
        SET @index = @index + 1;
    END;
    RETURN 0;
END;