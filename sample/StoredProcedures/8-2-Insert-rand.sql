﻿GO

CREATE PROCEDURE [insert_rand]
    @count INT = 3000
AS
BEGIN
    DECLARE @emp_no INT;
    DECLARE @index INT = 0;
    WHILE @index < @count
    BEGIN
        SET @emp_no = CAST(RAND(CHECKSUM(NEWID())) * 1000000 AS INT);
        INSERT [employee] (emp_no, emp_fname, emp_lname, dept_no)
        VALUES (@emp_no, 'Jane', 'Smith', 'd1');
        SET @index = @index + 1;
    END;
    RETURN 0;
END;