﻿CREATE TABLE [dbo].[employee]
(
    [emp_no] INT NOT NULL PRIMARY KEY, 
    [emp_fname] NVARCHAR(50) NULL, 
    [emp_lname] NVARCHAR(50) NULL, 
    [dept_no] NVARCHAR(50) NOT NULL,    
    CONSTRAINT [FK_employee_department] FOREIGN KEY ([dept_no]) REFERENCES [department]([dept_no])
)
