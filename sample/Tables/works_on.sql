﻿CREATE TABLE [dbo].[works_on]
(
    [emp_no] INT NOT NULL, 
    [project_no] NVARCHAR(50) NOT NULL, 
    [job] NVARCHAR(50) NULL, 
    [enter_date] DATE NULL, 
    CONSTRAINT [FK_works_on_project] FOREIGN KEY ([project_no]) REFERENCES [project]([project_no]),
    CONSTRAINT [FK_works_on_employee] FOREIGN KEY ([emp_no]) REFERENCES [employee]([emp_no])
)
