﻿CREATE TABLE [dbo].[project]
(
    [project_no] NVARCHAR(50) NOT NULL PRIMARY KEY, 
    [project_name] NVARCHAR(50) NULL, 
    [budget] INT NULL
)
