﻿CREATE TABLE [dbo].[department]
(
    [dept_no] NVARCHAR(50) NOT NULL PRIMARY KEY, 
    [dept_name] NVARCHAR(50) NULL, 
    [location] NVARCHAR(50) NULL
)
